NOTE: This project is part of the hackathon Hack for Sweden 2018.

This is a proof-of-concept demonstrating how public information can be placed on IPFS in order to make it resilient in a time of crisis. In this case we use data from the Swedish police. Futhermore we demonstrate that more complex data such as maps can be displayed, making it possible to e.g. display information about the location of shelters.

We have created a tool-chain which makes the process of interacting with the IPFS easier by creating a CLI, which can add files to IPFS, and publish pages to IPNS. The site can then be accessed via HTTP (as a proxy) or accessed directly on IPFS via that hash.

IPFS address (through gateway) of the current solution: [https://gateway.ipfs.io/ipns/QmcL7KRwGCPsABq4YhJA7XeGHuXuUQxjcTUg7Wk3QWWAKe](https://gateway.ipfs.io/ipns/QmcL7KRwGCPsABq4YhJA7XeGHuXuUQxjcTUg7Wk3QWWAKe)
